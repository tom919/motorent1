@extends('layouts.motorlayout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Motor Brand</div>
                @if (session('responsemessage'))
                          
                            <div class="alert alert-success">
                            {{session('responsemessage')}}
                            </div>
                            
                    @endif

                <div class="card-body">

                <div class="row">
                    <div class="col-12" >
                        <div class="pull-right p-2">
                        <button class="btn btn-info btn-outline btn-md" onclick="InputBrandPop()"><i class="fa fa-pencil-square"></i> New Brand</button>
                        </div>
                        <table class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($mtr as $data)
                                <tr style="height: 3em"><td>{{$data->id}}</td><td>{{$data->brand_name}}</td><td><a href="{{url('motorbranddelete')}}/{{$data->id}} " class="del"><button class="btn btn-primary btn-outline btn-sm">Delete</button></a></td></tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>

                </div>
                </div>
            </div>
        </div>
    </div>
</div>


  <!-- Modal -->
  <div class="modal fade" id="brandModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title pull-left">Add Motor Brand </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <form action="{{url('/motorbrandsave')}}" method="post" enctype="multipart/form-data">
                    @csrf

                  <div class="table-responsive" id = "div1" style = " width: 100%; margin-right: 10%;  background-color: white;">
                  <input type="text" class="form-control" name="brand" placeholder="enter brand name">
                  <div class="pull-right">
                    <button type="submit" value="submit" class="btn btn-primary m-md-3"><i class="fa fa-save"></i>&nbsp;save</button>
                  </div>
        </form>                           
        </div>
        </div>
    
      </div>

    </div>
  </div>
@endsection
