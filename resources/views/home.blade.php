@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Recent Booking</div>

                <div class="card-body">

                <div class="row">
                    <div >
                    <a href="{{url('/createbookingadmin')}}"><button class="btn btn-info btn-outline btn-md"><i class="fa fa-pencil-square"></i> New Booking</button></a>
                    <table id="example" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Motor</th>
                                    <th>Customer</th>
                                    <th>Book Date</th>
                                    <th>Time</th>
                                    <th>Rate</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                        </table>
    
                    </div>

                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
