<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MotorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('motor/motorindex');
    }

    public function createMotor()
    {
        return view('/motor/motorinput');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMotor(Request $request)
    {
        //validation
        $request->validate([
            'regNumber' => 'required',
            'type' => 'required',
            'year' => 'required',
            'brand' => 'required',
            'rate' => 'required',
            'pic' => 'required|image|mimes:jpeg,png,jpg',
            'status' => 'required'
        ],[
            'regNumber.required'=>'Registration Number Required',
            'type.required'=>'Motor Type Required',
            'year.required'=>'Motor Year Required',
            'brand.required'=>'Motor Brand Required',
            'rate.required'=>'Motor Rate Required',
            'status.required' => 'Motor status required'
        ]);

        //get data
        $regNumber=$request->input('regNumber');
        $type=$request->input('type');
        $year=$request->input('year');
        $brand=$request->input('brand');
        $rate=$request->input('rate');
        $status=$request->input('status');   
        $inputDate=date('Y-m-d H:i:s', time());
        // pic processing
        $pic = $regNumber.'.'.$request->pic->getClientOriginalExtension();
        $request->pic->move(public_path('motorpic'), $pic);
        //query
        $results = DB::select( DB::raw("CALL InsertMotor( '$regNumber', '$type', '$brand', '$year', '$rate', '$pic','$status')") );



        //response
        return redirect()->route('motor')->with('responsemessage',$results[0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ShowById($id)
    {

        $results = DB::select("CALL MotorDetail(?)",[$id]);
        return response()->json([$results[0]]);
        // return view('motor/motordetail',["data"=>$results[0]]);
    }
    /* display all data */
    public function ShowAll(Request $request)
    {

        $limit=10;
        $start=$request->input('start')*$limit;
        if($request->input('search.value'))
            {
                $search=$request->input('search.value');
            }
        else
            {
                $search='';
            }
        $results = DB::select("CALL ShowMotorAll(?,?,?)",[$start,$limit,$search] );
        $counter_listing=DB::table('motor')->count();
        $counter_filter=ceil($counter_listing/$limit);
        return response()->json(["data"=>$results,"draw"=>$request->input('draw'),"recordsTotal"=>$counter_listing,"recordsFiltered"=>$counter_filter]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editMotor($id)
    {

        $results = DB::select("CALL MotorDetail(?)",[$id]);
        return view('/motor/motoredit',["data"=>$results[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateMotor(Request $request)
    {
        //validation
        $request->validate([
            'regNumber' => 'required',
            'type' => 'required',
            'year' => 'required',
            'brand' => 'required',
            'rate' => 'required',
            'status' => 'required'
        ],[
            'regNumber.required'=>'Registration Number Required',
            'type.required'=>'Motor Type Required',
            'year.required'=>'Motor Year Required',
            'brand.required'=>'Motor Brand Required',
            'rate.required'=>'Motor Rate Required',
            'status.required' => 'Motor status required'
        ]);

        //get data
        $id=$request->input('id');
        $regNumber=$request->input('regNumber');
        $type=$request->input('type');
        $year=$request->input('year');
        $brand=$request->input('brand');
        $rate=$request->input('rate');
        $status=$request->input('status');   
        $inputDate=date('Y-m-d H:i:s', time());

        if($request->hasFile('pic')){
            $file = public_path('motorpic/').$request->input('picid');

            unlink($file);

          $pic = $regNumber.'.'.$request->pic->getClientOriginalExtension();
          $request->pic->move(public_path('motorpic'), $pic);
       } else{
           $pic = $request->input('picid');
       }



        $results = DB::select("CALL UpdateMotor(?,?,?,?,?,?,?,?)",[$id, $regNumber , $type, $brand, $year, $rate, $pic, $status]);
        return redirect()->route('motor')->with('responsemessage',$results[0]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMotor($id)
    {
        $mtr= DB::select("SELECT * FROM motor WHERE id='$id'");
        $file = public_path('motorpic/').$mtr[0]->pic;
        unlink($file);

        $results = DB::select("CALL DeleteMotor(?)",[$id]);
        return redirect()->route('motor')->with('responsemessage',$results[0]);
    }

    /* motor type */
    public function indexType(){
        $mtr= DB::select("SELECT id, name FROM motor_type ORDER BY id");
        return view('/motor/setting/motortype')->with('mtr',$mtr);
    }
    public function createType()
    {
        return view('/motor/motortype');
    }
    public function getType()
    {
        $results = DB::select("SELECT id, name FROM motor_type");
        return response()->json($results);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeType(Request $request)
    {
        //validation
        $request->validate([
            'type' => 'required'
        ],[
            'type.required'=>'Motor Type Required'
        ]);

        //get data
        $type=$request->input('type');

        //query
        $results = DB::select( "INSERT INTO motor_type(name) VALUES('$type') " );

        //response
        return redirect()->route('motortypesetting')->with('responsemessage','Motor Type Added');
    }


    public function destroyType($id)
    {
        $results = DB::select("DELETE FROM motor_type WHERE id='$id'");
        return redirect()->route('motortypesetting')->with('responsemessage','Motor Type Deleted');
    }

      /* motor brand */
    public function indexBrand(){
        $mtr= DB::select("SELECT id, brand_name FROM motor_brand ORDER BY id");
        return view('/motor/setting/motorbrand')->with('mtr',$mtr);
    }
    public function createBrand()
    {
        return view('/motor/motorbrand');
    }
    public function getBrand()
    {
        $results = DB::select("SELECT id, brand_name from motor_brand");
        return response()->json($results);
    }
    public function storeBrand(Request $request)
    {
        //validation
        $request->validate([
            'brand' => 'required'
        ],[
            'brand.required'=>'Motor Brand Required'
        ]);

        //get data
        $brand=$request->input('brand');

        //query
        $results = DB::select( "INSERT INTO motor_brand(brand_name) VALUES('$brand') " );

        //response
        return redirect()->route('motorbrandsetting')->with('responsemessage','Motor Brand Added');
    }


    public function destroyBrand($id)
    {
        $results = DB::select("DELETE FROM motor_brand WHERE id='$id'");
        return redirect()->route('motorbrandsetting')->with('responsemessage','Motor Brand Deleted');
    }

}
