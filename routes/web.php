<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::get('/motorbrandf','FrontController@getBrand');
Route::get('/motortypef','FrontController@getType');
Route::get('/motorgallery','FrontController@motorGallery');
Route::post('/motorbook','FrontController@motorBook');

Auth::routes([
    'register'=>false,
    'reset'=>false,
    'verify'=>false
    ]);

Route::get('/home', 'HomeController@index')->name('home');

//booking controller
Route::get('/createbookingadmin', 'BookingController@createBookingAdmin');


//motor controller
Route::get('/motor','MotorController@index')->name('motor');
Route::get('/motorshowall','MotorController@ShowAll');
Route::get('/motordetail/{id}','MotorController@ShowById');
Route::get('/motorinput','MotorController@createMotor');
Route::post('/motorsave','MotorController@storeMotor');
Route::get('/motoredit/{id}','MotorController@editMotor');
Route::post('/motorupdate','MotorController@updateMotor');
Route::get('/motordelete/{id}','MotorController@destroyMotor');
//motor type sub controler
Route::get('/motortypesetting','MotorController@indexType')->name('motortypesetting');
Route::get('/motortypeinput','MotorController@createType');
Route::post('/motortypesave','MotorController@storeType');
Route::get('/motortype','MotorController@getType');
Route::get('/motortypedelete/{id}','MotorController@destroyType');
//motor brand controller
Route::get('/motorbrandsetting','MotorController@indexBrand')->name('motorbrandsetting');
Route::get('/motorbrandinput','MotorController@createBrand');
Route::post('/motorbrandsave','MotorController@storeBrand');
Route::get('/motorbrand','MotorController@getBrand');
Route::get('/motorbranddelete/{id}','MotorController@destroyBrand');
//booking controller

//customer controller

//product controller

//article controller

//setting controller

